$(document).ready(() => {
  // HTML Vars
  const title = $('#title');
  const container = $('#container');
  // Login-Home invoke function
  function login() {
    // Login AJAX
    $.ajax({
      type: 'GET',
      url: '/html/login.html',
      success: loginLoad,
      error: handleError
    });
  }

  // Render login form
  function loginLoad(data: string) {
    title.html('Medi-App Login');
    container.html(data);
    if (!$('#container').is(':visible'))
      container.animate({ height: 'toggle' }, 500);
    // Form Submit
    $('#login-form').on('submit', (e) => {
      e.preventDefault();
      const loginData = new FormData($('#login-form')[0] as HTMLFormElement);
      $.ajax({
        type: 'POST',
        url: '/login',
        processData: false,
        contentType: false,
        data: loginData,
        success: loginSubmit,
        error: handleError
      });
    });

    // Sign-in invoke function
    $('#btn-sigin').click(() => {
      container.animate({ height: 'toggle' }, 500, () => {
        $.ajax({
          type: 'GET',
          url: 'html/sigin.html',
          success: siginLoad,
          error: handleError
        });
      });
    });
  }

  // Render sigin form
  function siginLoad(data: string) {
    title.html('Medi-App Registry');
    container.html(data);
    container.animate({ height: 'toggle' }, 500);
    $('#btnHome').click(() => {
      container.animate({ height: 'toggle' }, 500, login);
    });
    // Form Submit
    $('#sigin-form').on('submit', (e) => {
      e.preventDefault();
      if ($('#password1').val() === $('#password2').val()) {
        const siginData = new FormData($('#sigin-form')[0] as HTMLFormElement);
        $.ajax({
          type: 'POST',
          url: '/sigin',
          processData: false,
          contentType: false,
          data: siginData,
          success: siginSubmit,
          error: handleError
        });
      } else {
        $('#log-container').html(
          "<div class='alert alert-danger' role='alert'>" +
            'Las contraseñas no coinciden!' +
            '</div>'
        );
      }
    });
  }

  // Sigin request result
  function siginSubmit(data: string) {
    if (data === 'yes') {
      $.ajax({
        type: 'GET',
        url: '/html/success.html',
        success: (data2) => {
          container.html(data2);
          $('#btn-go-login').click(login);
        },
        error: handleError
      });
    } else {
      $('#log-container').html(
        "<div class='alert alert-danger' role='alert'>" + data + '</div>'
      );
    }
  }

  // Login request result
  function loginSubmit(data: string) {
    if (data === 'yes') {
      container.animate({ height: 'toggle' }, 300);
      setTimeout(() => (window.location.href = '/home'), 300);
    } else {
      $('#log-container').html(
        "<div class='alert alert-danger' role='alert'>" + data + '</div>'
      );
    }
  }

  // AJAX error log function
  function handleError(_jqXHR: any, _textStatus: any, error: string) {
    console.log(error);
  }

  login();
});
