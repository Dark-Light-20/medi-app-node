import mysql from "mysql";

const pool = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "medi-admin",
  password: "Medicamentos_123",
  database: "mediapp",
});

export default pool;
