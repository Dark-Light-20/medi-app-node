"use strict";
const invSettings = {
    language: {
        url: '/DataTables/Spanish.json'
    },
    ajax: {
        url: '/get-items',
        dataSrc: ''
    },
    columns: [
        { data: 'expediente' },
        { data: 'nombre' },
        { data: 'cantidad' },
        { data: 'descripcion' },
        {
            defaultContent: `<div class='text-center'>
            <div class='btn-group btn-group-sm'>
            <button class='btn btn-success btnAdd' data-toggle='modal' data-target='#modalAddSell' data-type='add'><i class='material-icons'>add_circle</i></button>
            <button class='btn btn-warning btnSell' data-toggle='modal' data-target='#modalAddSell' data-type='sell'><i class='material-icons'>remove_circle</i></button>
            <button class='btn btn-info btnInfo' data-toggle='modal' data-target='#modalInfo' id='btnInfo'><i class='material-icons'>info</i></button>
            <button class='btn btn-danger btnDelete' data-toggle='modal' data-target='#modalDel'><i class='material-icons'>delete</i></button>
            </div>
            </div>`
        }
    ],
    dom: 'lBfrtip',
    buttons: [
        {
            extend: 'copyHtml5',
            text: "<button class='btn btn-info'><i class='fas fa-copy'></button>",
            titleAttr: 'Copy',
            exportOptions: { columns: [0, 1, 2, 3] }
        },
        {
            extend: 'excelHtml5',
            text: "<button class='btn btn-success'><i class='fas fa-file-excel'></i></button>",
            titleAttr: 'Excel',
            exportOptions: { columns: [0, 1, 2, 3] }
        },
        {
            extend: 'pdfHtml5',
            text: "<button class='btn btn-danger'><i class='fas fa-file-pdf'></button>",
            titleAttr: 'PDF',
            exportOptions: { columns: [0, 1, 2, 3] }
        }
    ]
};
const salesSettings = {
    language: {
        url: '/DataTables/Spanish.json'
    },
    ajax: {
        url: '/get-sales',
        dataSrc: ''
    },
    columns: [
        { data: 'date' },
        { data: 'IDc' },
        { data: 'nombre' },
        { data: 'cnd' }
    ],
    dom: 'lBfrtip',
    buttons: [
        {
            extend: 'copyHtml5',
            text: "<button class='btn btn-info'><i class='fas fa-copy'></button>",
            titleAttr: 'Copy'
        },
        {
            extend: 'excelHtml5',
            text: "<button class='btn btn-success'><i class='fas fa-file-excel'></i></button>",
            titleAttr: 'Excel'
        },
        {
            extend: 'pdfHtml5',
            text: "<button class='btn btn-danger'><i class='fas fa-file-pdf'></button>",
            titleAttr: 'PDF'
        }
    ]
};
$(document).ready(() => {
    $('body').removeClass('primary-color-light');
    const invTable = $('#dataTable').DataTable(invSettings);
    const salesTable = $('#salesTable').DataTable(salesSettings);
    let expd;
    $('#container').animate({ height: 'toggle' }, 2000);
    $('#navbar').animate({ height: 'toggle' }, 2000);
    $('#btnRegistry').on('click', () => {
        const registryData = new FormData($('#registry-form')[0]);
        $.ajax({
            type: 'POST',
            url: '/registry-item',
            processData: false,
            contentType: false,
            data: registryData,
            success: (data) => {
                if (data === 'yes') {
                    invTable.ajax.reload();
                    $('#modalRegistry').modal('hide');
                }
                else {
                    $('#msgRegistry').html(`<div class='alert alert-danger'>Server Error!!! Fallo al registrar el item: ${data} </div>`);
                }
                $('#registry-form').trigger('reset');
            },
            error: (xhr) => {
                const errorMessage = `${xhr.status} : ${xhr.statusText}`;
                $('#msgRegistry').html(`<div class='alert alert-danger'>API Error!!! ${errorMessage} </div>`);
            }
        });
    });
    $('#modalRegistry').on('show.bs.modal', () => {
        $('#name').on('input', (e) => {
            const name = $(e.target).val().toUpperCase();
            if (name !== '' && name.length > 3) {
                $('#list').html('');
                $.ajax({
                    type: 'GET',
                    url: `https://www.datos.gov.co/resource/i7cb-raxc.json?$query=select%20distinct%20expediente,producto%20where%20producto%20like%20%27${name}%25%27`,
                    dataType: 'json',
                    success: (data) => {
                        let items = '';
                        $.each(data, (index, item) => {
                            items += `<div class='class list-group-item list-group-item-action medi-item'>${item.producto} | Exp: ${item.expediente}</div>`;
                        });
                        $('#list').html(items);
                    }
                });
            }
            else
                $('#list').html('');
        });
    });
    $(document).on('click', '.medi-item', (e) => {
        $('#list').html('');
        const data = $(e.target).text().split(' | ');
        $('#name').val(data[0]);
        $('#id-exp').val(data[1].substring(5));
    });
    $('#modalRegistry').on('hide.bs.modal', () => {
        $('#registry-form')[0].reset();
    });
    $('#modalAddSell').on('show.bs.modal', (e) => {
        const mType = $(e.relatedTarget).data('type');
        switch (mType) {
            case 'add':
                $('#modalTitle').html('Agregar Unidades');
                $('#btnModalAddSell')
                    .removeClass('btn-warning btn-danger')
                    .addClass('btn-primary');
                $('#btnModalAddSell').html('Agregar');
                break;
            case 'sell':
                $('#sellID').show();
                $('#modalTitle').html('Vender Unidades');
                $('#btnModalAddSell')
                    .removeClass('btn-primary btn-danger')
                    .addClass('btn-warning');
                $('#btnModalAddSell').html('Vender');
                break;
        }
    });
    $('#modalAddSell').on('hide.bs.modal', () => {
        $('#sellID').hide();
        $('#formAddSell')[0].reset();
    });
    $(document).on('click', '.btnAdd, .btnSell, .btnInfo, .btnDelete', (e) => {
        const row = $(e.target).closest('tr');
        expd = row.find('td:eq(0)').text();
        $('.expTxt').html(expd);
    });
    $(document).on('click', '#btnModalAddSell', () => {
        if (!$('#modalNum')[0].checkValidity()) {
            errorAlert();
            return;
        }
        const num = $('#modalNum').val();
        const type = $('#btnModalAddSell').html();
        let dataSend;
        switch (type) {
            case 'Agregar':
                dataSend = { op: type, cnd: num, exp: expd };
                break;
            case 'Vender':
                if (!$('#idc')[0].checkValidity()) {
                    errorAlert();
                    return;
                }
                const idClient = $('#idc').val();
                dataSend = { op: type, cnd: num, exp: expd, idc: idClient };
                break;
        }
        $.ajax({
            type: 'POST',
            url: '/update-item',
            data: dataSend,
            success: (data) => {
                if (data === 'yes') {
                    invTable.ajax.reload();
                    salesTable.ajax.reload();
                    $('#modalAddSell').modal('hide');
                }
                else {
                    $('#msgAddSell').html(`<div class='alert alert-danger'>Server Error!!! ${data} </div>`);
                }
            }
        });
    });
    $(document).on('click', '#btnInfo', (e) => {
        $.ajax({
            type: 'GET',
            url: `https://www.datos.gov.co/resource/i7cb-raxc.json?expediente=${expd}`,
            data: { $limit: 1 },
            dataType: 'json',
            success: (data) => {
                if (data.length > 0) {
                    $('#infoProd').html(data[0].producto);
                    $('#infoTitular').html(data[0].titular);
                    $('#infoReg').html(data[0].registrosanitario);
                    $('#infoFechEx').html(data[0].fechaexpedicion);
                    $('#infoFechVen').html(data[0].fechavencimiento);
                    $('#infoCnd').html(data[0].cantidadcum);
                    $('#infoDesc').html(data[0].descripcioncomercial);
                    $('#infoVia').html(data[0].viaadministracion);
                    $('#infoPrAc').html(data[0].principioactivo);
                    $('#infoUnd').html(data[0].cantidad);
                    $('#infoUndMd').html(data[0].unidadmedida);
                    $('#infoUndRf').html(data[0].unidadreferencia);
                }
                else {
                    $('#modalBodyInfo').html('');
                    $('#msgInfo').html(`<div class='alert alert-danger'>No se encontró información del item # ${expd}</div>`);
                }
            },
            error: (xhr) => {
                const errorMessage = `${xhr.status} : ${xhr.statusText}`;
                $('#msgInfo').html(`<div class='alert alert-danger'>API Error!!! ${errorMessage} </div>`);
            }
        });
    });
    $('#modalInfo').on('hide.bs.modal', () => {
        $('#msgInfo').html('');
        $.ajax({
            type: 'GET',
            url: '/html/infoFields.html',
            success: (data) => {
                $('#modalBodyInfo').html(data);
            }
        });
    });
    $(document).on('click', '#btnModalDel', () => {
        $.ajax({
            type: 'POST',
            url: '/delete-item',
            data: { exp: expd },
            success: (data) => {
                if (data === 'yes') {
                    invTable.ajax.reload();
                    $('#modalDel').modal('hide');
                }
                else {
                    $('#msgDel').html(`<div class='alert alert-danger'>Server Error!!! ${data} </div>`);
                }
            }
        });
    });
    $('.btnMenus').click(() => {
        $('#inventory').animate({ height: 'toggle' }, 500);
        $('#sales').animate({ height: 'toggle' }, 500);
        $('#btnInventory').toggleClass('btn-secondary');
        $('#btnInventory').toggleClass('btn-info');
        $('#btnInventory').prop('disabled', (i, v) => !v);
        $('#btnSales').toggleClass('btn-secondary');
        $('#btnSales').toggleClass('btn-info');
        $('#btnSales').prop('disabled', (i, v) => !v);
    });
    function errorAlert() {
        $('#msgAddSell').html(`<div class='alert alert-danger'>Error en los datos</div>`);
    }
});
