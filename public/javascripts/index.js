"use strict";
$(document).ready(() => {
    const title = $('#title');
    const container = $('#container');
    function login() {
        $.ajax({
            type: 'GET',
            url: '/html/login.html',
            success: loginLoad,
            error: handleError
        });
    }
    function loginLoad(data) {
        title.html('Medi-App Login');
        container.html(data);
        if (!$('#container').is(':visible'))
            container.animate({ height: 'toggle' }, 500);
        $('#login-form').on('submit', (e) => {
            e.preventDefault();
            const loginData = new FormData($('#login-form')[0]);
            $.ajax({
                type: 'POST',
                url: '/login',
                processData: false,
                contentType: false,
                data: loginData,
                success: loginSubmit,
                error: handleError
            });
        });
        $('#btn-sigin').click(() => {
            container.animate({ height: 'toggle' }, 500, () => {
                $.ajax({
                    type: 'GET',
                    url: 'html/sigin.html',
                    success: siginLoad,
                    error: handleError
                });
            });
        });
    }
    function siginLoad(data) {
        title.html('Medi-App Registry');
        container.html(data);
        container.animate({ height: 'toggle' }, 500);
        $('#btnHome').click(() => {
            container.animate({ height: 'toggle' }, 500, login);
        });
        $('#sigin-form').on('submit', (e) => {
            e.preventDefault();
            if ($('#password1').val() === $('#password2').val()) {
                const siginData = new FormData($('#sigin-form')[0]);
                $.ajax({
                    type: 'POST',
                    url: '/sigin',
                    processData: false,
                    contentType: false,
                    data: siginData,
                    success: siginSubmit,
                    error: handleError
                });
            }
            else {
                $('#log-container').html("<div class='alert alert-danger' role='alert'>" +
                    'Las contraseñas no coinciden!' +
                    '</div>');
            }
        });
    }
    function siginSubmit(data) {
        if (data === 'yes') {
            $.ajax({
                type: 'GET',
                url: '/html/success.html',
                success: (data2) => {
                    container.html(data2);
                    $('#btn-go-login').click(login);
                },
                error: handleError
            });
        }
        else {
            $('#log-container').html("<div class='alert alert-danger' role='alert'>" + data + '</div>');
        }
    }
    function loginSubmit(data) {
        if (data === 'yes') {
            container.animate({ height: 'toggle' }, 300);
            setTimeout(() => (window.location.href = '/home'), 300);
        }
        else {
            $('#log-container').html("<div class='alert alert-danger' role='alert'>" + data + '</div>');
        }
    }
    function handleError(_jqXHR, _textStatus, error) {
        console.log(error);
    }
    login();
});
