class MediUser {
    private id:number;
    private name: string;
    private logged: boolean;

    constructor(){
        this.id = 0;
        this.name = 'Nan';
        this.logged = false;
    }

    public logUser(session : Express.Session){
        this.id = (session.userId) ? session.userId : 0;
        this.name = (session.userName) ? session.userName : 'Nan';
        this.logged = (session.userLog) ? session.userLog : false;
    }

    public deleteUser(session : Express.Session){
        delete session.userId;
        delete session.userName;
        delete session.userLog;
        this.id = 0;
        this.name = 'Nan';
        this.logged = false;
    }

    public isLogged(){
        return this.logged;
    }

    public getName() {
        return this.name;
    }
}

export default MediUser;