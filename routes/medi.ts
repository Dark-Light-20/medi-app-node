import express from 'express';
import mysql, { MysqlError } from 'mysql';
import pool from '../DB/database';
import bcrypt from 'bcrypt';
import moment from 'moment';
import MediUser from '../models/user';

const router = express.Router();
const currentUser = new MediUser();

function checkAlreadySession(
  req: express.Request,
  res: express.Response
): boolean {
  if (req.session) currentUser.logUser(req.session);
  if (currentUser.isLogged()) {
    res.redirect('/home');
    return true;
  }
  return false;
}

function checkSession(req: express.Request, res: express.Response): boolean {
  if (req.session) currentUser.logUser(req.session);
  if (!currentUser.isLogged()) {
    res.redirect('/');
    return false;
  }
  return true;
}

router.get('/', (req, res) => {
  if (checkAlreadySession(req, res)) return;
  res.render('index', { title: 'Medi-App', navbarText: 'Medi-App' });
});

router.post('/login', (req, res) => {
  if (checkAlreadySession(req, res)) return;
  if (req.body.username === undefined && req.body.password === undefined)
    res.send('Error en los datos de variable, intente de nuevo');

  res.setHeader('content-type', 'text/plain');

  const user: string = req.body.username;
  const pswd: string = req.body.password;

  let sql: string = 'SELECT ID, pwd FROM USERS WHERE user=?';
  const insert: string[] = [user];

  sql = mysql.format(sql, insert);

  pool.query(sql, (error: MysqlError, results: any[]) => {
    if (error) {
      res.send(String(error.sqlMessage));
      throw error;
    }
    if (results.length > 0) {
      bcrypt.compare(pswd, results[0].pwd, (err: Error, result: boolean) => {
        if (err) {
          res.send(String(err));
          throw err;
        }
        if (result) {
          if (req.session) {
            req.session.userId = results[0].ID;
            req.session.userName = user;
            req.session.userLog = true;
            currentUser.logUser(req.session);
          }
          res.send('yes');
        } else res.send('Contraseña incorrecta');
      });
    } else res.send('Usuario no existe');
  });
});

router.get('/logout', (req, res) => {
  if (req.session) {
    currentUser.logUser(req.session);
    if (currentUser.isLogged()) {
      currentUser.deleteUser(req.session);
    }
  }
  res.redirect('/');
});

router.post('/sigin', (req, res) => {
  if (
    req.body.username === undefined &&
    req.body.password1 === undefined &&
    req.body.password2 === undefined
  )
    res.send('Error en los datos de variable, intente de nuevo');

  res.setHeader('content-type', 'text/plain');

  const user: string = req.body.username;
  const pwd1: string = req.body.password1;
  const pwd2: string = req.body.password2;

  if (pwd1 !== '' && pwd1 === pwd2) {
    bcrypt.hash(pwd1, 10, (err, hash) => {
      let sql = 'INSERT INTO USERS(user, pwd) VALUES(?, ?)';
      const inserts = [user, hash];
      sql = mysql.format(sql, inserts);
      pool.query(sql, (error: MysqlError) => {
        if (error) {
          if (error.errno === 1062) {
            res.send('El usuario ya existe');
            return;
          }
          res.send(String(error.sqlMessage));
          throw error;
        }
        res.send('yes');
      });
    });
  } else res.send('Las contraseñas no coinciden');
});

router.get('/home', (req, res) => {
  if (req.session) currentUser.logUser(req.session);
  if (currentUser.isLogged()) {
    res.render('home', {
      title: 'Inventario - Medi-App',
      navbarText: `Bienvenido ${currentUser.getName()}`
    });
    return;
  } else res.redirect('/');
});

router.get('/get-items', (req, res) => {
  if (!checkSession(req, res)) return;
  const sql = 'SELECT * FROM MEDICAMENTOS';
  pool.query(sql, (error: MysqlError, results: any[]) => {
    if (error) {
      res.send(String(error.sqlMessage));
      throw error;
    }
    res.json(results);
  });
});

router.post('/registry-item', (req, res) => {
  if (!checkSession(req, res)) return;
  if (
    req.body['id-exp'] === undefined &&
    req.body.name === undefined &&
    req.body.desc === undefined &&
    req.body.can === undefined
  ) {
    res.send('Error en los datos de variable, intente de nuevo');
    return;
  }

  if (req.body.can < 1) {
    res.send('Cantidad incorrecta');
    return;
  }

  let sql = 'INSERT INTO MEDICAMENTOS VALUES(?, ?, ?, ?)';
  const inserts = [
    req.body['id-exp'],
    req.body.name,
    req.body.can,
    req.body.desc
  ];
  sql = mysql.format(sql, inserts);
  pool.query(sql, (error: MysqlError) => {
    if (error) {
      if (error.errno === 1062) res.send('El item ya existe');
      res.send(String(error.sqlMessage));
      throw error;
    }
    res.send('yes');
  });
});

router.post('/update-item', (req, res) => {
  if (!checkSession(req, res)) return;
  if (
    req.body.op === undefined &&
    req.body.cnd === undefined &&
    req.body.exp === undefined
  ) {
    res.send('Error en los datos de variable, intente de nuevo');
    return;
  }

  let sql: string;
  let inserts: any[];

  switch (req.body.op) {
    case 'Agregar':
      // Buy quantity
      sql = 'UPDATE MEDICAMENTOS SET cantidad=cantidad+? WHERE expediente=?';
      inserts = [req.body.cnd, req.body.exp];
      sql = mysql.format(sql, inserts);
      pool.query(sql, (error: MysqlError) => {
        if (error) {
          res.send(String(error.sqlMessage));
          throw error;
        }
        res.send('yes');
      });
      break;
    case 'Vender':
      if (req.body.idc === undefined || req.body.idc.length > 10) {
        res.send('Error en los datos de variable, intente de nuevo');
        return;
      }
      // Get actual quantity
      let check = 'SELECT cantidad FROM MEDICAMENTOS WHERE expediente=?';
      inserts = [req.body.exp];
      check = mysql.format(check, inserts);
      let cndAct: number;
      pool.query(check, (error: MysqlError, results: any[]) => {
        if (error) {
          res.send(String(error.sqlMessage));
          throw error;
        }
        cndAct = results[0].cantidad;
        // Check sell
        if (req.body.cnd <= cndAct!) {
          // Update quantity
          sql =
            'UPDATE MEDICAMENTOS SET cantidad=cantidad-? WHERE expediente=?';
          inserts = [req.body.cnd, req.body.exp];
          sql = mysql.format(sql, inserts);
          pool.query(sql, (error2: MysqlError) => {
            if (error2) {
              res.send(String(error2.sqlMessage));
              throw error2;
            }
          });
          // Add SALES registry
          sql = 'INSERT INTO SALES VALUES(?, ?, ?, ?)';
          inserts = [
            req.body.idc,
            req.body.exp,
            req.body.cnd,
            moment().format('YYYY-MM-DD HH:mm:ss')
          ];
          sql = mysql.format(sql, inserts);
          pool.query(sql, (error2: MysqlError) => {
            if (error2) {
              res.send(String(error2.sqlMessage));
              throw error2;
            }
            res.send('yes');
          });
        } else res.send('La cantidad que trata de vender es mayor a la actual');
      });
      break;
    default:
      res.send('Error en los datos de variable, intente de nuevo');
  }
});

router.post('/delete-item', (req, res) => {
  if (!checkSession(req, res)) return;
  if (req.body.exp === undefined) {
    res.send('Error en los datos de variable, intente de nuevo');
    return;
  }

  let sql = 'DELETE FROM MEDICAMENTOS WHERE expediente=?';
  const inserts = [Number(req.body.exp)];
  sql = mysql.format(sql, inserts);
  pool.query(sql, (error: MysqlError) => {
    if (error) {
      res.send(String(error.sqlMessage));
      throw error;
    }
    res.send('yes');
  });
});

router.get('/get-sales', (req, res) => {
  if (!checkSession(req, res)) return;
  const sql =
    'SELECT IDc, nombre, cnd, DATE_FORMAT(date, "%d/%m/%Y %H:%i:%s") as date FROM SALES INNER JOIN MEDICAMENTOS WHERE exp=expediente';
  pool.query(sql, (error: MysqlError, results: any[]) => {
    if (error) {
      res.send(String(error.sqlMessage));
      throw error;
    }
    res.json(results);
  });
});

router.get('*', (req, res) => {
  res.render('error', { title: 'Medi-App' });
});

export default router;
