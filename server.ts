// Imports
import express from 'express';
import path from 'path';
import multer from 'multer';
import session from 'express-session';

// App
const app = express();

// Multer
const upload = multer();

// Load utilities
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  secret: 'medi-app',
  resave: false,
  saveUninitialized: false
}));
// For non-file multipart forms
app.use(upload.none());

// Views engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Import Routes
import mediRouter from './routes/medi';

// Use Routes
app.use(mediRouter);

app.listen(8000, () => {
  console.log('Running on port 8000');
});
